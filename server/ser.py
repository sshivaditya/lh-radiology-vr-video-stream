
import random
import socket
import threading
import json
import os
from flask import Flask, request, send_from_directory, flash, request, redirect, url_for, render_template
from gevent.pywsgi import WSGIServer
import asyncio
import websockets
from flask_cors import CORS

IP = "192.168.1.11"
PORT = 9999

# set the project root directory as the static folder
app = Flask(__name__)
UPLOAD_FOLDER = './uploads'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER 
@app.route('/', methods=['POST'])
def DownloadFile():
    # request.form to get form parameter
    audFile = request.files["audFile"]
    filename = request.form.to_dict()["filename"]
    #outF = open(filename+".wav", "wb")
    #outF.write(audFile)
    audFile.save(os.path.join(app.config['UPLOAD_FOLDER'],filename+".wav"))
    return ''

@app.route('/video/<filename>')
def display_video(filename):
	#rint('display_video filename: ' + filename)
	return redirect(url_for('static', filename='media/' + filename), code=301)

@app.route('/video/html/<file>')
def video(file):
    return render_template('stream.html',file=file)

@app.route('/uploads/<path:filename>', methods=['GET', 'POST'])
def download(filename):
    uploads = os.path.join(app.root_path, app.config['UPLOAD_FOLDER'])
    return send_from_directory(directory=uploads, filename=filename)

CONNECTIONS = set()

async def sockert_send(websocket,msg):
    await asyncio.sleep(1)
    await websocket.send(msg)

async def handler(websocket, path):
    while True:
        try:
            data = await websocket.recv()
            CONNECTIONS.add(websocket)
            if data[0:5]=="PAUSE":
                for connection in CONNECTIONS:
                    await sockert_send(connection,data)
                await asyncio.sleep(random.random() * 2 + 1)
            elif data[0:5]=="PLAY0":
                for connection in CONNECTIONS:
                    await sockert_send(connection,data)
                await asyncio.sleep(random.random() * 2 + 1)
            print(CONNECTIONS)
        except websockets.exceptions.ConnectionClosed:
            print("Connection Terminated")
            CONNECTIONS.remove(websocket)
        reply = f"Data recevied as: {data}!"
        reply = bytes(reply,"utf-8")
        print("{}: {}".format(websocket, data))
        await websocket.send(reply)

class SocketServer(socket.socket):
    clients = []
    
    def __init__(self):
        socket.socket.__init__(self)
        #To silence- address occupied!!
        self.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.bind(('192.168.1.11', 8052))
        self.listen(5)
        self.thread = None

    def run(self):
        print("Server started")
        try:
            self.accept_clients()
        except Exception as ex:
            print(ex)
        finally:
            print("Server closed")
            for client in self.clients:
                client.close()
            self.close()

    def accept_clients(self):
        client_count = 1
        while 1:
            (clientsocket, address) = self.accept()
            #Adding client to clients list
            self.clients.append(clientsocket)
            #Client Connected
            self.onopen(clientsocket,client_count)
            id = client_count
            client_count+=1
            #Receiving data from client
            threading.Thread(target=self.recieve, args=(clientsocket,id)).start()

    def recieve(self, client,id):
        while 1:
            data = client.recv(1024)
            if data == b'':
                break
            print(data)
            #Message Received
            print(data)
            self.onmessage(client, data,id)
        #Removing client from clients list
        self.clients.remove(client)
        #Client Disconnected
        self.onclose(client)
        #Closing connection with client
        client.close()
        #Closing thread
        #thread.join()
        print(self.clients)

    def broadcast(self, message,id):
        #Sending message to all clients
        #s = bytes("""{"SenderData":{"ID":3,"Name":"User3"},"Data":"Hi Default Message on Oculus Quest"}""", 'utf-8')
        data = {}
        data["SenderData"] = {"ID":str(id),"Name":"User" + str(id)}
        data["Data"] = message.decode("utf-8")
        d = json.dumps(data)
        print(d)
        s = bytes(d,"utf-8")
        for client in self.clients:
            client.send(s)

    def onopen(self, client,id):
        #s = bytes("""{"SenderData":{"ID":10,"Name":"User13"},"Data":"Client Connected"}""", 'utf-8') 
        data = {}
        data["SenderData"] = {"ID":str(id),"Name":"User" + str(id)}
        data["Data"] = b"Client Connected".decode("utf-8")
        d = json.dumps(data)
        print(d)
        s = bytes(d,"utf-8")
        client.send(s)
        print("Connected with " + str(client.getpeername()))

    def onmessage(self, client, message,id):
        self.broadcast(message,id)

    def onclose(self, client):
        pass

def main():
    server = SocketServer()
    server.run()
def flask_main():
    CORS(app)
    http_server = WSGIServer((IP, PORT), app)
    http_server.serve_forever()

new_loop = asyncio.new_event_loop()
start_server = websockets.serve(handler, "192.168.1.11", 8765, loop=new_loop)

def start_loop(loop, server):
    loop.run_until_complete(server)
    loop.run_forever()

if __name__ == "__main__":
    threading.Thread(target=flask_main).start()
    threading.Thread(target=main).start()
    t = threading.Thread(target=start_loop, args=(new_loop, start_server))
    t.start()
    
    