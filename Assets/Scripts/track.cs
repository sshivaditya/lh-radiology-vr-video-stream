using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.EventSystems;
using NativeWebSocket;
using System;
using TMPro;

public class track : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

    public VideoPlayer video;
    public VideoPlayer hidden;
    public TMP_InputField tmp_input;
    public TMP_InputField IPInputField;
    public TMP_InputField PortInputField;
    WebSocket websocket;
    public Button play;
    public Button pause;
    public Button Sync;
    public Button Connect;
    public Button Disconnect;
    Slider tracking;
    bool slide = false;
    bool seekDone = false;
    string server_uri;

    async void Start()
    {
        tracking = GetComponent<Slider>();
        server_uri = "ws://" + IPInputField.text + ":" + PortInputField.text;
        string video_uri = "http://" + IPInputField.text + ":9999/video/test.mp4";
        video.url = video_uri;
        hidden.url = video_uri;
        Debug.Log(server_uri);
        websocket = new WebSocket(server_uri);
        video.Play();
        video.Pause();
        hidden.Play();
        hidden.Pause();
        video.seekCompleted += Video_seekCompleted;
        play.GetComponent<Button>().onClick.AddListener(delegate {PlayEvent(); });
        Sync.GetComponent<Button>().onClick.AddListener(delegate { SyncEvent(); });
        Connect.GetComponent<Button>().onClick.AddListener(async delegate { await websocket.Connect(); });
        Disconnect.GetComponent<Button>().onClick.AddListener(async delegate { await websocket.Close(); });
        websocket.OnOpen += async () =>
        {
            Debug.Log("Connection open!");
            if (websocket.State == WebSocketState.Open)
            {
                // Sending plain text
                await websocket.SendText("Connected to Server");
            }
        };

        websocket.OnError += (e) =>
        {
            Debug.Log("Error! " + e);
        };

        websocket.OnClose += (e) =>
        {
            Debug.Log("Connection closed!");
        };
        websocket.OnMessage += (bytes) =>
        {
            var message = System.Text.Encoding.UTF8.GetString(bytes);
            Debug.Log("OnMessage! " + message);
            if (string.Equals("PAUSE", message.Substring(0, 5)))
            {
                video.Pause();
                hidden.Pause();
                int timestamp = int.Parse(message.Substring(5));
                Debug.Log(video.time);
                Debug.Log(video.frame);
                Debug.Log(timestamp);
                UpdateVideoPlayerToFrame(timestamp);
                video.Pause();
                hidden.Pause();
                video.frame = (long)timestamp;
                hidden.frame = (long)timestamp;
                video.Play();
                hidden.Play();
                video.Pause();
                hidden.Pause();
                Debug.Log(video.time);
                Debug.Log(video.frame);
                Debug.Log(hidden.time);
                Debug.Log(hidden.frame);
            }
            if (string.Equals("PLAY0", message.Substring(0, 5)))
            {
                video.Play();
                hidden.Play();
            }
        };


        

    }

    private void Video_seekCompleted(VideoPlayer source)
    {
        Debug.Log("Seeked to Server Point");
    }

    async void PlayEvent()
    {
        Double time_val = video.GetComponent<VideoPlayer>().time;
        await websocket.SendText("Play Event"+time_val.ToString());

    }
    void SyncEvent()
    {
        video.Pause();
        hidden.Pause();
        video.frame = hidden.frame;
        video.Play();
        hidden.Play();
        Debug.Log(video.time);
        Debug.Log(video.frame);
        Debug.Log(hidden.time);
        Debug.Log(hidden.frame);
    }
   

    public void OnPointerDown(PointerEventData a)
    {
        slide = true;
    }

    public void OnPointerUp(PointerEventData a)
    {
        float frame = (float)tracking.value * (float)video.frameCount;
        video.frame = (long)frame;
        slide = false;
    }
    void Update()
    {
        #if !UNITY_WEBGL || UNITY_EDITOR
                websocket.DispatchMessageQueue();
        #endif
        if (!slide && video.isPlaying)
        {
            tracking.value = (float)video.frame / (float)video.frameCount;
        }

    }


    private async void OnApplicationQuit()
    {
        Debug.Log("Close Call Triggered");
        await websocket.Close();
    }

    void seekCompleted(VideoPlayer par)
    {
        StartCoroutine(WaitToUpdateRenderTextureBeforeEndingSeek());
    }

    public void UpdateVideoPlayerToFrame(int frame)
    {
        //If you are currently seeking there is no point to seek again.
        if (!seekDone)
            return;

        // You should pause while you seek for better stability
        video.Pause();

        video.frame = frame;
        seekDone = false;
    }

    IEnumerator WaitToUpdateRenderTextureBeforeEndingSeek()
    {
        yield return new WaitForEndOfFrame();
        seekDone = true;
    }


}
